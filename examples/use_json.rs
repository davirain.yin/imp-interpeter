use serde::{Deserialize, Serialize};
use std::env;
use std::fs::File;
use std::io::Read;

/// read json file
pub fn read_json(path: &str) -> String {
    let mut file = File::open(path).unwrap();
    let mut buff = String::new();
    file.read_to_string(&mut buff).unwrap();
    buff
}

/// Test type
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Foo {
    pub name: String,
    age: u32,
}

/// 测试读取简单的json文件
#[test]
fn test_read_json() {
    let current_dir = env::current_dir().unwrap();
    let path = format!("{}/input/test.json", current_dir.to_str().unwrap());
    let foo = read_json(&path);
    let foo: Foo = serde_json::from_str(&foo).unwrap();
    assert_eq!("Jane", foo.name);
}

fn main() {}
