use crate::primitives::firstlogicexpression::FirstLogicExpression;
use crate::primitives::outputks::KsStates;
use crate::primitives::outputks::{LRule, OutputKs, TrasationR};
use crate::primitives::state::get_all_states;
use crate::primitives::state::State;
use crate::utils::execute::{ExecutePointer, ExecuteStateHahMap};
use std::env::var;
use std::fs::File;
use std::collections::VecDeque;
use crate::utils::iofile::write_data_to_file;

const NUM_RANGE: [u8; 3] = [0, 1, 2];

/// main logic
pub struct Logic {
    /// 初始集合的所有状态
    state: State,
    /// current state
    current_state: State,
    /// 状态的开始 pc = m_start
    start_pc: String,
    /// 当前的状态
    current_pc: String,
    /// 存储的状态转换
    execute_state_hash_map: ExecuteStateHahMap,
    /// execute pointer
    execute_pointer: ExecutePointer,
    /// output ks structure
    output_ks: OutputKs,
    /// 状态的结束
    end_pc: String,
    /// 状态执行队列
    /// (0: first, 1: end)
    state_queue: VecDeque<(State, State)>,
}

impl Logic {
    /// 静态构造函数
    pub fn new(state: &State, seq: &[FirstLogicExpression]) -> Self {
        let mut execute_state_hash_map = ExecuteStateHahMap::build(seq);
        let mut execute_pointer = ExecutePointer::build(seq);

        // 得到所有变量的个数
        let var_len = seq.get(0).unwrap().get_start_states().get_variables_len();
        let variables_names = seq.get(0).unwrap().get_start_states().get_all_variables();
        let all_states = get_all_states(var_len, &NUM_RANGE);

        let mut vec_ks_state = Vec::new();

        for elem in all_states.iter() {
            let temp = KsStates::build(variables_names.clone(), elem);
            vec_ks_state.push(temp);
        }

        let mut output_ks = OutputKs::new()
            .init_s0(state.clone().into())
            .init_s(vec_ks_state);

        Self {
            state: state.clone(),
            current_state: state.clone(),
            start_pc: String::new(),
            current_pc: String::new(),
            execute_state_hash_map,
            execute_pointer,
            output_ks,
            end_pc: String::new(),
            state_queue: VecDeque::new(),
        }
    }

    pub fn update_first_log_expression(&mut self, state: State, seq: &[FirstLogicExpression]) {
        assert_ne!(0, seq.len());
        let mut n_seq: Vec<FirstLogicExpression> = Vec::new();
        for (index, val) in seq.iter().enumerate() {
            if index == 0 {
                let mut new_val = val.clone();
                new_val.update_initial_value(state.clone());

                n_seq.push(new_val);
            } else {
                n_seq.push(val.clone());
            }
        }

        for elem in n_seq.iter() {
            log::info!("elem: {}", elem);
            println!("elem: {}", elem);
        }

        self.execute_pointer.update_seq(&n_seq);
    }

    /// 设置 execute state HashMap
    pub fn execute_state_hash_map(self, execute_state_hash_map: ExecuteStateHahMap) -> Self {
        Self {
            execute_state_hash_map,
            ..self
        }
    }

    /// 设置ExecutePointer
    pub fn execute_pointer(self, execute_pointer: ExecutePointer) -> Self {
        Self {
            execute_pointer,
            ..self
        }
    }

    /// 设置OutputKs
    pub fn output_ks(self, output_ks: OutputKs) -> Self {
        Self { output_ks, ..self }
    }

    /// 设置状态的起始pc
    pub fn start_pc(self, start_pc: &str) -> Self {
        Self {
            start_pc: start_pc.to_string(),
            ..self
        }
    }

    /// 设置状态的结束pc
    pub fn end_pc(self, end_pc: &str) -> Self {
        Self {
            end_pc: end_pc.to_string(),
            ..self
        }
    }

    pub fn get_all_states(&self) -> &Vec<KsStates> {
        self.output_ks.get_all_s_states()
    }

    /// 用于在更新为一个新的状态后继续循环执行逻辑
    pub fn update_state(&mut self, state: State) {
        self.state.update_state(state.clone());
        self.current_state.update_state(state);
    }

    pub fn update_s0_state(&mut self, state: State) {
        self.output_ks.update_s0(state);
    }

    /// 主要的流程逻辑
    /// 注意这里的current_pc的变迁
    pub fn process(&mut self) {
        // 清空状态队列
        self.state_queue.clear();

        // 初始化当前pc为
        self.current_pc = self.get_start_pc().to_string();

        loop {
            // 当current pc 为m_end结束
            if self.current_pc == self.get_end_pc() {
                log::info!("⚠️ current_pc : {}", self.current_pc.clone());
                break;
            }

            // 得到当前current pc的下一个pc的键值对
            // 因为可以有多个分支的存在
            log::info!("⚠️ current_pc : {}", self.current_pc.clone());
            let all_state = self
                .execute_state_hash_map
                .get_state(self.current_pc.clone());
            match all_state {
                None => {
                    println!("💥 all state is empty!!");
                    break;
                }
                // 对拿到的匹配的键值对进行遍历
                Some(val) => {
                    // 遍历键值对
                    for elem in val.iter() {
                        // 根据 execute pointer辅助结构体,通过键值KV,得到 first logic expression 结构体
                        let formal = self.execute_pointer.get_logic_expression(elem);

                        // 根据传入的状态state,
                        // 判断当前的表达式 运算后为true 或者还是false
                        // self.state是传入的状态
                        if formal.is_work(self.current_state.clone()) {
                            // 判断 表达式返回的是true
                            // 根据 first logic expression更新计算后的状态
                            // 输入的是变迁之前的状态集合
                            // 输出的是计算后的状态集合
                            log::info!("⚠️ before update {}", self.current_state.clone());

                            let ret = formal.update_state(self.current_state.clone());
                            log::info!("⚠️ after update {}", ret.clone());

                            // 构造 Trasaction R 变迁关系
                            let temp_r =
                                TrasationR::new(self.current_state.clone().into(), ret.clone().into());
                            log::info!("🐰 trasation R {}", temp_r.clone());

                            // 构造 LRule 关系
                            let temp_l = LRule::new(self.current_state.clone().into(), ret.clone().into());
                            log::info!("🐰 L Rule {}", temp_l.clone());

                            // 加入状态队列
                            self.state_queue.push_back((self.current_state.clone(), ret.clone().into()));

                            // 更新状态
                            self.current_state.update_state(ret.clone());

                            if formal.is_expression() {
                                // 更新 trasaction R 关系集合
                                self.output_ks.update_r(temp_r);

                                // 更新 LRule 关系集合
                                self.output_ks.update_l(temp_l);
                            }

                            // 更新 current_pc 为 formal 的last_pc
                            self.current_pc = formal.get_last_pc().into();
                        } else {
                            // 如果当前运算错误继续执行下一个表达式
                            continue;
                        }
                    }
                }
            }
        }
    }

    pub fn get_single_state_s_path(&self) -> &str {
        "s.txt"
    }

    pub fn get_single_state_r_path(&self) -> &str {
        "r.txt"
    }

    pub fn get_single_output_ks(&self) -> &str {
        "output_ks.txt"
    }

    /// 获取起始的start_pc
    pub fn get_start_pc(&self) -> &str {
        &self.start_pc
    }

    /// 获取结束的end_pc
    pub fn get_end_pc(&self) -> &str {
        &self.end_pc
    }

    /// 获取当前的current_pc
    pub fn get_current_pc(&self) -> &str {
        &self.current_pc
    }

    /// 显示OutputKs
    pub fn display_result(&self) {
        println!("{}", self.output_ks);
    }

    pub fn get_output_ks_result(&self) -> String {
        format!("{}", self.output_ks)
    }

    /// 将OutputKs 的r 输出到文件， 默认保存为r.txt
    pub fn to_ks_r_file(&self, path: &str) {
        // let scope_path = format!("{}/{}", path, name);
        self.output_ks.write_output_ks_r_to_file(path);
    }

    pub fn get_state_queue_format(&self) -> String {
        let mut result = String::new();
        for (first_state, second_state) in self.state_queue.iter() {
            let first_state = first_state.get_output_format();
            log::info!("first state : {}", &first_state);
            let second_state = second_state.get_output_format();
            log::info!("second state: {}", &second_state);

            result.push_str(&first_state);
            result.push('\n');
            result.push_str(&second_state);
            result.push('\n');
        }
        result
    }

    pub fn to_pretty_ks_r_file(&self, path: &str) {
        // 初始状态输出格式
        let start_state = self.state.clone().get_output_format();
        write_data_to_file(path, start_state.as_bytes());
        write_data_to_file(path, "\n".as_bytes());
        // write_data_to_file(path, "\n".as_bytes());
        // 转移队列
        let state_queue = self.get_state_queue_format();
        write_data_to_file(path, state_queue.as_bytes());
        write_data_to_file(path, "\n".as_bytes());
        // write_data_to_file(path, "\n".as_bytes());
    }

    pub fn to_ks_s_file(&self, path: &str) {
        // let scope_path = format!("{}/{}", path, name);
        self.output_ks.write_output_ks_s_to_file(path);
    }
}
