//! # input json file
//!
//!
//! # output json file
//!

#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_imports)]
#![allow(unused_mut)]
#![allow(private_in_public)]
#![allow(unused_must_use)]

#[macro_use]
extern crate serde_json;
mod cli;
mod imp_parser;
mod logic;
mod primitives;
mod utils;
mod pre_process;

#[macro_use]
extern crate nom;
use env_logger;

use crate::primitives::state::State;
use cli::Opt;
use logic::Logic;
use primitives::firstlogicexpression::FirstLogicExpression;
use std::path::PathBuf;
use structopt::StructOpt;
use crate::utils::iofile;


fn main() {
    env_logger::init();

    let opt: Opt = Opt::from_args();
    println!("{:#?}", opt);

    let start_pc = "m".to_string();
    let end_pc = "m'".to_string();

    // 读取输入的数据
    let path = opt.get_input_file();
    let ret_result = iofile::read_json(&path);
    let first_logic_expressions_states: Vec<FirstLogicExpression> =
        serde_json::from_str(&ret_result).unwrap();
    log::info!(
        "🎄 first logic expressions states : {:?}",
        &first_logic_expressions_states
    );

    for elem in first_logic_expressions_states.iter() {
        log::info!("elem: {}", elem);
        println!("elem: {}", elem);
    }

    assert!(
        !first_logic_expressions_states.is_empty(),
        "input data is empty"
    );

    // 单个状态的启动
    let start_state = first_logic_expressions_states
        .get(0)
        .unwrap()
        .get_start_states();

    let mut main_logic = Logic::new(&start_state, &first_logic_expressions_states)
        .start_pc(&start_pc)
        .end_pc(&end_pc);

    main_logic.process();

    main_logic.display_result();

    // 得到输出文件的路径
    let output_file_path = opt.get_output_file();

    // 获得单状态文件设置的文件名
    let r_default_path = main_logic.get_single_state_r_path();
    let s_default_path = main_logic.get_single_state_s_path();

    let output_ks_path = main_logic.get_single_output_ks();

    let output_file_path_r = format!("{}/{}", output_file_path, r_default_path);
    let output_file_path_s = format!("{}/{}", output_file_path, s_default_path);
    let output_file_path_output_ks = format!("{}/{}", output_file_path, output_ks_path);

    std::fs::remove_file(&output_file_path_r);
    std::fs::remove_file(&output_file_path_s);
    std::fs::remove_file(&output_file_path_output_ks);

    main_logic.to_ks_r_file(&output_file_path_r);
    // main_logic.to_pretty_ks_r_file(&output_file_path_r);
    main_logic.to_ks_s_file(&output_file_path_s);
    let result_output_ks = main_logic.get_output_ks_result();
    iofile::write_data_to_file(&output_file_path_output_ks, result_output_ks.as_bytes());

    // 循环等多个状态的启动
    log::info!("👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌");
    let all_states = main_logic.get_all_states();

    // log::info!("❌ all states = {:#?}", all_states);
    log::info!("💡 all state len = {}", all_states.len());

    // assert!(!all_states.len());
    assert_ne!(all_states.len(), 0);

    // 得到输出文件的路径
    let output_file_path = opt.get_output_file();

    let multi_r_name = String::from("multi_r.txt");
    let multi_s_name = String::from("multi_s.txt");
    let multi_output_ks_name = String::from("multi_ks.txt");

    let multi_output_file_path_r = format!("{}/{}", output_file_path, multi_r_name);
    let multi_output_file_path_s = format!("{}/{}", output_file_path, multi_s_name);
    let multi_output_file_path_output_ks = format!("{}/{}", output_file_path, multi_output_ks_name);

    std::fs::remove_file(&multi_output_file_path_r);
    std::fs::remove_file(&multi_output_file_path_s);
    std::fs::remove_file(&multi_output_file_path_output_ks);

    // 第一行的语句忽略过去
    let mut main_logic = Logic::new(
        &all_states[0].clone().into(),
        &first_logic_expressions_states[..],
    )
    .start_pc(&start_pc)
    .end_pc(&end_pc);

    log::info!("🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍state : {}", all_states[0].clone());
    for state in all_states.iter() {
        log::info!("🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍🐍");

        main_logic.update_state(state.clone().into());
        main_logic.process();

        log::info!(
            "🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲🌲 state: {}",
            state.clone()
        );
        main_logic.update_s0_state(state.clone().into());
        main_logic.display_result();

        main_logic
            .update_first_log_expression(state.clone().into(), &first_logic_expressions_states);
        log::info!("🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨🎨");

        let result_output_ks = main_logic.get_output_ks_result();
        iofile::write_data_to_file(&multi_output_file_path_output_ks, result_output_ks.as_bytes());
        iofile::write_data_to_file(&multi_output_file_path_output_ks, format!("\n").as_bytes());

        // main_logic.to_pretty_ks_r_file(&multi_output_file_path_r);
    }
    // main_logic.display_result();
    main_logic.to_ks_r_file(&multi_output_file_path_r);
    main_logic.to_ks_s_file(&multi_output_file_path_s);
}
