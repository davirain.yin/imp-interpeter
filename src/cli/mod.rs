use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "imp-ks",
    about = "用于将一阶逻辑谓词公式输入输出为KS, 支持人类可读的格式"
)]
pub struct Opt {
    /// Input file
    #[structopt(parse(from_os_str))]
    input: PathBuf,

    /// Output file, stdout if not present
    #[structopt(parse(from_os_str))]
    output: Option<PathBuf>,

    /// Where to write the output: to `stdout` or `file`
    #[structopt(short)]
    out_type: String,

    /// File name: only required when `out-type` is set to `file`
    #[structopt(name = "FILE", required_if("out-type", "file"))]
    file_name: Option<String>,
}

impl Opt {
    pub fn get_input_file(&self) -> &str {
        self.input.to_str().unwrap()
    }

    pub fn get_output_file(&self) -> &str {
        match self.output {
            None => return "",
            Some(ref file) => return file.to_str().unwrap(),
        }
    }
}
