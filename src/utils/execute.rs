use crate::primitives::firstlogicexpression::FirstLogicExpression;
use crate::primitives::outputks::{LRule, OutputKs, TrasationR};
use crate::primitives::state::State;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

/// execute pointer
pub struct ExecutePointer {
    hash_map: BTreeMap<KV, FirstLogicExpression>,
}

impl ExecutePointer {
    pub fn new() -> Self {
        Self {
            hash_map: BTreeMap::new(),
        }
    }

    pub fn update_seq(&mut self, seq: &[FirstLogicExpression]) {
        for elem in seq.iter() {
            let kv = KV {
                first: elem.get_pc_first().to_string(),
                second: elem.get_pc_last().to_string(),
            };
            self.hash_map.insert(kv, elem.clone());
        }
    }

    pub fn build(seq: &[FirstLogicExpression]) -> Self {
        let mut hash_map: BTreeMap<KV, FirstLogicExpression> = BTreeMap::new();
        for elem in seq.iter() {
            let kv = KV {
                first: elem.get_pc_first().to_string(),
                second: elem.get_pc_last().to_string(),
            };
            hash_map.insert(kv, elem.clone());
        }
        Self { hash_map }
    }

    pub fn get_logic_expression(&self, key: &KV) -> &FirstLogicExpression {
        self.hash_map.get(key).unwrap()
    }
}

/// first 状态的开始
/// second 状态的结束
#[derive(Eq, Hash, PartialEq, Ord, PartialOrd)]
pub struct KV {
    first: String,
    second: String,
}

impl KV {
    pub fn get_first(&self) -> &str {
        &self.first
    }
}

/// 存储状态的转换
pub struct ExecuteStateHahMap {
    /// key 表示一个要转移的一个状态的开始状态
    /// value 表示与下一状态相关联了的状态
    hash_map: BTreeMap<String, Vec<String>>,
}

impl ExecuteStateHahMap {
    pub fn new() -> Self {
        Self {
            hash_map: BTreeMap::new(),
        }
    }

    /// 根据一阶逻辑公式构造
    pub fn build(seq: &[FirstLogicExpression]) -> Self {
        let mut hash_map: BTreeMap<String, Vec<String>> = BTreeMap::new();

        for elem in seq.iter() {
            let key = elem.get_pc_first().to_string();
            let value = elem.get_pc_last().to_string();

            if let Some(val) = hash_map.get_mut(&key) {
                val.push(value);
            } else {
                hash_map.insert(key, vec![value]);
            }
        }

        Self { hash_map }
    }

    /// 根据输入的一个状态得到他的下一个状态，把所有的结果汇总成一个结构体
    pub fn get_state(&self, key: String) -> Option<Vec<KV>> {
        let mut result: Vec<KV> = Vec::new();
        if let Some(val) = self.hash_map.get(&key) {
            for elem in val.iter() {
                let value = KV {
                    first: key.clone(),
                    second: elem.clone(),
                };
                result.push(value);
            }
        } else {
            return None;
        }
        Some(result)
    }
}
