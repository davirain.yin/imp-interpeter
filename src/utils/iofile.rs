use serde::{Deserialize, Serialize};
use std::env;
use std::fs::File;
use std::io::{Read, Write};

use crate::primitives::firstlogicexpression::{FirstLogicExpression, PayloadData, SameOrNo, KV};

/// read json file
pub fn read_json(path: &str) -> String {
    let mut file = File::open(path).unwrap();
    let mut buff = String::new();
    file.read_to_string(&mut buff).unwrap();
    buff
}

/// 将数据写入文件
pub fn write_data_to_file(path: &str, data: &[u8]) {
    let mut exist_file = std::fs::OpenOptions::new().append(true).open(path);
    match exist_file {
        Ok(mut file) => {
            log::info!("✅ open file success! {:?}", file);
            file.write_all(data).expect("write failed");
        }
        Err(_) => {
            log::info!("❎ 文件不存在需要创建");
            let mut file = File::create(path).expect("create failed");
            log::info!("✅ create file success! {:?}", file);
            file.write_all(data).expect("write failed");
        }
    }
}

#[test]
fn test_read_json_first_logic_expression() {
    let current_dir = env::current_dir().unwrap();
    let path = format!("{}/source/input.json", current_dir.to_str().unwrap());
    let buff = read_json(&path);
    let temp: Vec<FirstLogicExpression> = serde_json::from_str(&buff).unwrap();
    assert_eq!(3, temp.len());
}

// serde example first logic expression
#[test]
fn test_serde_a_json1() {
    let temp = FirstLogicExpression {
        payload_data: PayloadData::InitialValue(vec![
            KV {
                name: "x".into(),
                value: "0".into(),
            },
            KV {
                name: "y".into(),
                value: "0".into(),
            },
        ]),
        pc_first: "m".into(),
        pc_last: "l1".into(),
    };
    let json = serde_json::to_string_pretty(&temp).unwrap();
}

//  x = y + 1 ^ Same(V) ^ pc = l1 ^ pc' = m'
#[test]
fn test_serde_a_json_example2() {
    let temp = FirstLogicExpression {
        payload_data: PayloadData::Exp {
            expression: Some("x = y + 1".into()),
            same: SameOrNo::Same,
        },
        pc_first: "l1".into(),
        pc_last: "m'".into(),
    };
    println!("{}", temp);
    let json = serde_json::to_string_pretty(&temp).unwrap();
    println!("{:?}", json);
}

// Same(V/x) ^ pc = l1 ^ pc' = m'
#[test]
fn test_serde_a_json_example3() {
    let temp = FirstLogicExpression {
        payload_data: PayloadData::Exp {
            expression: None,
            same: SameOrNo::Div("x".into()),
        },
        pc_first: "l1".into(),
        pc_last: "m'".into(),
    };
    println!("{}", temp);
    let json = serde_json::to_string_pretty(&temp).unwrap();
    println!("{:?}", json);
}
