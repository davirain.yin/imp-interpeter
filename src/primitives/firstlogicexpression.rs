use super::state::State;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use std::fmt;
use std::hash::Hash;

use crate::imp_parser::evaluator::environment::*;
use crate::imp_parser::evaluator::object::*;
use crate::imp_parser::evaluator::*;
use crate::imp_parser::lexer::token::*;
use crate::imp_parser::lexer::Lexer;
use crate::imp_parser::parser::ast::*;
use crate::imp_parser::parser::Parser;
use crate::primitives::state::Tuple;

const CONST_NUM: [u8; 3] = [0, 1, 2];

/// input first logic expression
#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct FirstLogicExpression {
    /// Payload Data
    pub payload_data: PayloadData,
    /// pc = m
    pub pc_first: String,
    /// pc' = m'
    pub pc_last: String,
}

impl FirstLogicExpression {
    pub fn get_pc_first(&self) -> &str {
        &self.pc_first
    }

    pub fn get_pc_last(&self) -> &str {
        &self.pc_last
    }

    /// 执行 payload中的表达式 返回bool
    pub fn is_work(&self, state: State) -> bool {
        // execute  expression return is works
        match self.payload_data {
            PayloadData::Exp {
                ref expression,
                ref same,
            } => {
                match expression {
                    // 语句为空返回true
                    None => return true,
                    Some(exp) => {
                        let before_exp = format!("{}", state);
                        // 需要前置处理因为 表达式有的可能是逻辑表达式
                        let exp = self.payload_data.pre_process();
                        log::info!("🐰 exp ## {} ## ", exp);
                        let sum_exp = format!("{}{}", before_exp, exp);
                        let ret = self.get_result(&sum_exp.as_bytes());
                        log::info!("🙅 Object : {}", ret);
                        match ret {
                            // 计算出值来返回为true
                            Object::Integer(_) => {
                                return true;
                            }
                            // 计算里面的逻辑表达式
                            Object::Boolean(val) => return val,
                            _ => panic!("❎错误不可以到达"),
                        }
                    }
                }
            }
            _ => return true,
        }
    }

    /// 获取last_pc
    pub fn get_last_pc(&self) -> &str {
        &self.pc_last
    }

    // 根据计算表达式更新状态
    pub fn update_state(&self, state: State) -> State {
        log::info!("🐰 state ## {} ## ", state);

        match self.payload_data {
            PayloadData::Exp {
                ref expression,
                ref same,
            } => {
                match expression {
                    // 语句为空 没有修改状态
                    None => {
                        log::info!("❌ None");
                        return state.clone();
                    }
                    Some(exp) => {
                        //得到状态的格式化
                        let before_exp = format!("{}", state);
                        log::info!("🐰 before_exp ## {} ##", before_exp);

                        // 需要前置处理因为 表达式有的可能是逻辑表达式
                        let exp = self.payload_data.pre_process();
                        log::info!("🐰 exp ## {} ## ", exp);

                        // 计算是加上前置的初始化状态
                        let sum_exp = format!("{}{}", before_exp, &exp);
                        log::info!("🐰 sum_exp ## {} ##", sum_exp);

                        // 提取计算之后的那个变量是哪一变量
                        let var_name = self.get_result_var_name(&exp);
                        log::info!("🐰 var_name ## {} ##", var_name);

                        let ret = self.get_result(&sum_exp.as_bytes());
                        match ret {
                            // 计算表达式会修改状态
                            Object::Integer(val) => {
                                log::info!("😄 val : {}", val);
                                let val = val % CONST_NUM.len() as i64;
                                log::info!("🐰 val ## {} ##", val);
                                let ret_state =
                                    state.update_states_by_single_var(&var_name, val as u8);
                                log::info!("🐰 ret_state ## {} ##", ret_state);
                                return ret_state;
                            }
                            // 逻辑表达式不会修改状态
                            Object::Boolean(val) => {
                                log::info!("🐰 boolean # {} #", val);
                                return state.clone();
                            }
                            _ => panic!("❎错误不可以到达"),
                        }
                    }
                }
            }
            _ => return state.clone(),
        }
    }

    fn get_result_var_name(&self, exp: &str) -> String {
        log::info!("🐰 exp {}", exp);
        let first_index = exp.find("let");
        log::info!("🐰 first_index : {:?}", first_index);
        let second_index = exp.find("=");
        log::info!("🐰 second_index: {:?}", second_index);

        match first_index {
            None => return String::new(),
            Some(index1) => {
                // exp[0..index].to_string()
                match second_index {
                    None => return String::new(),
                    Some(index2) => {
                        log::info!(
                            "🐰 var_name : {}",
                            exp[index1 + 3..index2].trim().to_string()
                        );
                        return exp[index1 + 3..index2].trim().to_string();
                    }
                }
            }
        }
    }

    /// 计算结果
    fn get_result(&self, input: &[u8]) -> Object {
        let (_, r) = Lexer::lex_tokens(input).unwrap();
        let tokens = Tokens::new(&r);
        let (_, result_parse) = Parser::parse_tokens(tokens).unwrap();
        let mut evaluator = Evaluator::new();
        let eval = evaluator.eval_program(&result_parse);
        eval
    }

    // 获得起始的状态
    pub fn get_start_states(&self) -> State {
        self.payload_data.get_start_state()
    }

    pub fn is_expression(&self) -> bool {
        self.payload_data.is_expression()
    }

    pub fn update_initial_value(&mut self, state: State) {
        self.payload_data.update_initial_value(state)
    }
}

impl std::fmt::Display for FirstLogicExpression {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} pc = {} ^ pc' = {}",
            self.payload_data, self.pc_first, self.pc_last
        )
    }
}

#[derive(Serialize, Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum PayloadData {
    /// expression, same(V/{i})
    /// a = n; n:(0..2)
    /// a = b;
    /// a = a + b;
    /// a = a * b;
    /// a = a - b;
    /// a == b
    /// a <= b
    /// !a
    /// a && b
    /// a || b
    Exp {
        expression: Option<String>,
        same: SameOrNo,
    },
    /// x = 0, y = 0, z = 0, .....
    InitialValue(Vec<KV>),
}

impl PayloadData {
    pub fn update_initial_value(&mut self, state: State) {
        match *self {
            PayloadData::InitialValue(ref mut val) => {
                assert_eq!(val.len(), state.state.len());
                for elem in val.iter_mut().zip(state.state.iter()) {
                    elem.0.update(elem.1.clone());
                }
            }
            _ => panic!("invalid error!"),
        }
    }

    /// 获得起始状态
    pub fn get_start_state(&self) -> State {
        match *self {
            PayloadData::InitialValue(ref val) => {
                let mut state = Vec::new();
                for elem in val.iter() {
                    let name = elem.name.clone();
                    let value = elem.value.parse::<u8>().unwrap();
                    let tmp = Tuple::build(name, value);
                    state.push(tmp);
                }
                return State { state };
            }
            _ => panic!("❎ invaild error!"),
        }
    }

    /// 获得所有的变量名
    pub fn get_all_variables(&self) -> Vec<String> {
        // 起始状态中包含了所有的变量名
        let states = self.get_start_state();

        let result = states.get_all_variables();

        result
    }

    pub fn pre_process(&self) -> String {
        match *self {
            PayloadData::Exp {
                ref expression,
                ref same,
            } => {
                // let index2 = expression.as_ref().unwrap().find("<=");
                let index = expression.as_ref().unwrap().find("=");
                match index {
                    None => return expression.as_ref().unwrap().to_string(),
                    Some(val) => {
                        let n_index = expression.as_ref().unwrap().find(|c: char| {
                            c == '<' || c == '>'
                        } );
                        if n_index.is_some() {
                            return expression.as_ref().unwrap().to_string();
                        }else {
                            return format!("let {};", expression.as_ref().unwrap());
                        }
                    }
                }
            }
            _ => return String::new(),
        }
    }

    pub fn is_expression(&self) -> bool {
        return match *self {
            PayloadData::Exp { .. } => true,
            PayloadData::InitialValue(_) => false,
        };
    }
}

#[derive(Serialize, Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum SameOrNo {
    Same,
    Div(String),
}

impl fmt::Display for PayloadData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            // 输出有关表达式的
            PayloadData::Exp { expression, same } => {
                // 当expression不是none是输出
                if expression.is_some() {
                    if expression.as_ref().unwrap() != "" {
                        write!(f, "{} ^ ", expression.as_ref().unwrap())?;
                    }
                }

                match same {
                    SameOrNo::Same => write!(f, "Same(V) ^")?,
                    SameOrNo::Div(val) => write!(f, "Same(V/{}) ^", val)?,
                }

                write!(f, "")
            }
            // 输出初始化状态
            PayloadData::InitialValue(val) => {
                for elem in val.iter() {
                    write!(f, "{} ^ ", elem)?;
                }
                write!(f, "")
            }
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct KV {
    pub name: String,
    pub value: String,
}

impl KV {
    fn update(&mut self, val: Tuple) {
        self.name = val.get_name().to_string();
        self.value = val.get_value().to_string();
    }
}
impl fmt::Display for KV {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} = {}", self.name, self.value)
    }
}

#[test]
fn test_display_kv() {
    let kv = KV {
        name: "x".into(),
        value: "1".into(),
    };

    assert_eq!(format!("{}", kv), "x = 1".to_string());
}
