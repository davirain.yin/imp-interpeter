use super::state::State;
use crate::utils::iofile::write_data_to_file;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use std::fmt;

/// output Ks structure
#[derive(Serialize, Deserialize, Clone)]
pub struct OutputKs {
    d: Vec<InitValue>,
    s: Vec<KsStates>,
    s0: KsStates,
    r: HashSet<TrasationR>,
    l: HashSet<LRule>,
}

impl OutputKs {
    /// fn new() -> Self;
    pub fn new() -> Self {
        Self {
            d: vec![0, 1, 2].into_iter().map(|val| val.into()).collect(),
            s: Vec::new(),
            s0: KsStates::new(),
            r: HashSet::new(),
            l: HashSet::new(),
        }
    }

    /// fn init_d(self, d: Vec<InitValue>) -> Self;
    pub fn init_d(self, d: Vec<InitValue>) -> Self {
        Self { d, ..self }
    }

    /// fn init_s(self, s: Vec<KsStates>) -> Self;
    pub fn init_s(self, s: Vec<KsStates>) -> Self {
        Self { s, ..self }
    }

    /// fn init_s0(self, s0: KsStates) -> Self;
    pub fn init_s0(self, s0: KsStates) -> Self {
        Self { s0, ..self }
    }

    /// fn init_r(self, r: HashSet<TrasationR>) -> Self;
    pub fn init_r(self, r: HashSet<TrasationR>) -> Self {
        Self { r, ..self }
    }

    /// fn init_l(self, l: Vec<LRule>) -> Self;
    pub fn init_l(self, l: HashSet<LRule>) -> Self {
        Self { l, ..self }
    }

    pub fn update_s0(&mut self, state: State){
        self.s0.update(state);
    }

    /// fn update_r(&mut self, trasaction_r: TrasactionR);
    pub fn update_r(&mut self, trasaction_r: TrasationR) {
        log::info!("🐰 trasaction r : {}", trasaction_r);
        self.r.insert(trasaction_r);
    }

    /// fn update_l(&mut self, l_rule: LRule);
    pub fn update_l(&mut self, l_rule: LRule) {
        log::info!("🐰 rule r: {}", l_rule);
        self.l.insert(l_rule);
    }

    pub fn write_output_ks_s_to_file(&self, path: &str) {
        log::info!("✅  ks s path: {}", path);

        for elem in self.s.iter() {
            let name = format!("{}\n", elem.get_output_format());
            write_data_to_file(path, name.as_bytes());
        }
    }

    pub fn write_output_ks_r_to_file(&self, path: &str) {
        log::info!("✅ ks r path: {}", path);

        for elem in self.r.iter() {
            let first = format!("{}\n", elem.key.get_output_format());
            write_data_to_file(path, first.as_bytes());
            let second = format!("{}\n", elem.value.get_output_format());
            write_data_to_file(path, second.as_bytes());
        }
    }

    pub fn get_all_s_states(&self) -> &Vec<KsStates> {
        &self.s
    }
}

impl fmt::Display for OutputKs {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // display d
        write!(f, "D = {{")?;
        for elem in self.d.iter().enumerate() {
            if elem.0 == self.d.len() - 1 {
                write!(f, "{}", elem.1)?;
            } else {
                write!(f, "{},", elem.1)?;
            }
        }
        write!(f, "}}\n")?;

        // display s
        write!(f, "S = D x D = {{")?;
        for elem in self.s.iter().enumerate() {
            if elem.0 == self.s.len() - 1 {
                write!(f, "{}", elem.1)?;
            }else {
                write!(f, "{},", elem.1)?;
            }
        }
        write!(f, "}}\n")?;

        // display s0
        write!(f, "s0 = {}\n", self.s0)?;

        // display r
        write!(f, "R = {{")?;
        for elem in self.r.iter().enumerate() {
            if elem.0 == self.r.len() - 1{
                write!(f, "{}", elem.1)?;
            }else {
                write!(f, "{},", elem.1)?;
            }
        }
        write!(f, "}}\n")?;

        // display l
        write!(f, "L = {{")?;
        for elem in self.l.iter().enumerate() {
            if elem.0 == self.l.len() - 1 {
                write!(f, "{}", elem.1)?;
            }else {
                write!(f, "{},", elem.1)?;
            }
        }
        write!(f, "}}\n")
    }
}

#[derive(Serialize, Deserialize, Clone, Hash, PartialOrd, PartialEq, Eq)]
pub struct LRule {
    key: KsStates,
    value: KsStates,
}

impl LRule {
    pub fn new(key: KsStates, value: KsStates) -> Self {
        Self { key, value }
    }
}

impl fmt::Display for LRule {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({} -> {})", self.key, self.value)
    }
}

#[derive(Serialize, Deserialize, Clone, Hash, PartialOrd, PartialEq, Eq)]
pub struct TrasationR {
    key: KsStates,
    value: KsStates,
}

impl TrasationR {
    pub fn new(key: KsStates, value: KsStates) -> Self {
        Self { key, value }
    }
}

impl fmt::Display for TrasationR {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({} -> {})", self.key, self.value)
    }
}

// 用于存储状态集合
#[derive(Serialize, Deserialize, Clone, PartialOrd, PartialEq, Hash, Eq, Debug)]
pub struct KsStates {
    pub states: State,
}

impl KsStates {
    pub fn new() -> Self {
        Self {
            states: State::new(),
        }
    }

    pub fn update(&mut self, state: State) {
        self.states.update_state(state);
    }

    pub fn get_output_format(&self) -> String {
        self.states.get_output_format()
    }

    pub fn build(names: Vec<String>, val: &[u8]) -> Self {
        Self {
            states: State::build(names, val),
        }
    }
}

impl From<State> for KsStates {
    fn from(state: State) -> Self {
        Self { states: state }
    }
}

impl fmt::Display for KsStates {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "(")?;
        for elem in self.states.state.iter().enumerate() {
            if elem.0 == self.states.state.len() - 1 {
                write!(f, "{}", elem.1.get_value())?;
            }else {
                write!(f, "{},", elem.1.get_value())?;
            }
        }
        write!(f, ")")
    }
}

#[derive(Serialize, Debug, Deserialize, Clone)]
struct InitValue {
    num: u8,
}

impl fmt::Display for InitValue {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.num)
    }
}

impl From<u8> for InitValue {
    fn from(val: u8) -> Self {
        Self { num: val }
    }
}

#[test]
fn test_init_value() {
    let initval = InitValue { num: 0 };
    println!("{}", initval);
}
