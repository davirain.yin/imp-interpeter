use crate::primitives::outputks::KsStates;
use serde::{Deserialize, Serialize};

#[derive(Clone, Deserialize, Serialize, PartialOrd, PartialEq, Hash, Eq, Debug)]
pub struct State {
    pub state: Vec<Tuple>,
}

impl State {
    pub fn new() -> Self {
        Self { state: Vec::new() }
    }

    pub fn get_variables_len(&self) -> usize {
        self.state.len()
    }

    // 得到所有的变量
    pub fn get_variables_sorted(&self) -> Vec<String> {
        let mut result = Vec::new();
        for elem in self.state.iter() {
            result.push(elem.name.clone());
        }
        result.sort();
        result
    }

    // 从变量名和值中进行构造
    pub fn build(names: Vec<String>, values: &[u8]) -> Self {
        let mut state = Vec::new();
        assert_eq!(names.len(), values.len());

        for (name, value) in names.iter().zip(values.iter()) {
            let temp_tuple: Tuple = Tuple::build(name.into(), value.to_owned());
            state.push(temp_tuple);
        }
        Self { state }
    }

    // 构造所有的状态
    pub fn build_multi_state(names: Vec<String>, states: Vec<Vec<u8>>) -> Vec<State> {
        let mut state = Vec::new();

        for value in states.iter() {
            let ret = Self::build(names.clone(), value);
            state.push(ret);
        }
        state
    }

    /// 用于在改变不同的初始化状态时使用
    pub fn update_state(&mut self, state: State) {
        log::info!("✅ state: {}", &state);

        for index in 0..self.state.len() {
            self.state[index].update(state.state[index].value);
        }
    }

    // 改变单个值的状态
    pub fn update_states_by_single_var(&self, name: &str, value: u8) -> State {
        log::info!("✅ name : {}, value: {}", name, value);

        let mut state = Vec::new();
        for elem in self.state.iter() {
            if elem.name == name {
                let tuple = Tuple::build(name.to_string(), value);
                state.push(tuple);
            } else {
                state.push(elem.clone());
            }
        }
        Self { state }
    }

    /// 获得所有的变量名
    pub fn get_all_variables(&self) -> Vec<String> {
        let mut result = Vec::new();
        for elem in self.state.iter() {
            let name = elem.name.clone();
            result.push(name);
        }
        log::info!("✅ all variables : {:?}", result.clone());
        result
    }

    pub fn get_output_format(&self) -> String {
        let mut result = String::new();
        for elem in self.state.iter() {
            let val = elem.value;
            result.push_str(&val.to_string());
            result.push(',');
        }
        log::info!("✅ result : {}", result.clone());
        log::info!(
            "✅ result remove , : {}",
            result[..result.len() - 1].to_string()
        );
        // remove ,
        result[..result.len() - 1].to_string()
    }
}

impl From<KsStates> for State {
    fn from(state: KsStates) -> Self {
        Self {
            state: state.states.state,
        }
    }
}

impl std::fmt::Display for State {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for elem in self.state.iter() {
            write!(f, "let {} = {}; ", elem.name, elem.value)?;
        }
        write!(f, "")
    }
}

#[derive(Clone, Deserialize, Serialize, PartialOrd, PartialEq, Eq, Hash, Debug)]
pub struct Tuple {
    name: String,
    value: u8,
}

impl Tuple {
    pub fn new(name: String) -> Self {
        Self { name, value: 0u8 }
    }

    pub fn build(name: String, value: u8) -> Self {
        Self { name, value }
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn get_value(&self) -> u8 {
        self.value
    }

    pub fn update(&mut self, value: u8) {
        self.value = value
    }
}

impl From<(&&str, &u8)> for Tuple {
    fn from(val: (&&str, &u8)) -> Self {
        Self {
            name: val.0.to_string(),
            value: val.1.to_owned(),
        }
    }
}

/// 获取所有的状态空间
pub fn get_all_states(num_len: usize, array: &[u8]) -> Vec<Vec<u8>> {
    assert!(num_len <= 26);

    if num_len == 1 {
        let mut result = Vec::new();
        for elem in array.iter() {
            let tmp = vec![*elem];
            result.push(tmp);
        }
        return result;
    } else {
        let mut result = Vec::new();
        let mut ret = get_all_states(num_len - 1, array).clone();
        for n_elem in array.iter() {
            for elem in ret.iter() {
                let mut temp = elem.clone();
                temp.push(*n_elem);
                result.push(temp);
            }
        }
        return result;
    }
}

#[test]
fn test_get_all_states() {
    let ret = get_all_states(1, &[0, 1, 2]);
    println!("ret = {:?}", ret);
    let ret = get_all_states(2, &[0, 1, 2]);
    println!("ret = {:?}", ret);
    // let ret = get_all_states(3, &[0, 1, 2]);
    // println!("ret = {:#?}", ret);
}

#[test]
fn test_display_state() {
    let names = vec!["x".to_string(), "y".to_string(), "z".to_string()];
    let values = vec![0, 1, 2];

    let state = State::build(names, &values);

    println!("{}", state);
}

#[test]
fn test_mod_expresion() {
    let ret = (2 + 3) % 3;
    println!("ret = {}", ret);
}
