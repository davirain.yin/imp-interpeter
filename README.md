# imp-interpeter

## Rust 安装

使用 Rustup（推荐）
您似乎正在运行 macOS、Linux 或其它类 Unix 系统。要下载 Rustup 并安装 Rust，请在终端中运行以下命令，然后遵循屏幕上的指示。如果您在 Windows 上，请参见 “其他安装方式”。
```shell
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```


## imp-interpeter 使用方法

### 执行

```shell
# debug 模式下执行
$ cargo run 

# release 模式下执行
$ cargo run --release
```

### 生成的帮助信息

```shell
$ cargo run -- -h

imp-ks 0.1.0
用于将一阶逻辑谓词公式输入输出为KS, 支持人类可读的格式

USAGE:
    imp-ks <input> -o <out-type> [ARGS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -o <out-type>        Where to write the output: to `stdout` or `file`

ARGS:
    <input>     Input file
    <output>    Output file, stdout if not present
    <FILE>      File name: only required when `out-type` is set to `file`
```

### 生成的可执行文件存在

```shell
# debug 模式的
./target/debug/imp-interpeter

# release 模式的
./target/release/imp-interpeter
```

### 使用方式



### 测试一

```shell

## debug执行这个语句或者下面的
$ RUST_LOG=info cargo run -- ./input/input_copy.json  ./output -o FILE

$ cargo run -- ./input/input_copy.json  ./output -o FILE

# release执行
$ RUST_LOG=info cargo run --release  -- ./input/input_copy.json  ./output -o FILE

$ cargo run --release -- ./input/input_copy.json  ./output -o FILE
```
### 测试二


```shell
## debug执行这个语句或者下面的
$ RUST_LOG=info cargo run  -- ./input/order.json  ./output -o FILE

$ cargo run -- ./input/order.json  ./output -o FILE

## release执行
$ RUST_LOG=info cargo run --release  -- ./input/order.json  ./output -o FILE

$ cargo run  --release -- ./input/order.json  ./output -o FILE
```

### 测试三

```shell
## debug执行
$ RUST_LOG=info cargo run -- input_file_path  output_file_path  -o FILE

$ cargo run -- input_file_path  output_file_path -o FILE

## release 执行
$ RUST_LOG=info cargo run --release -- input_file_path  output_file_path  -o FILE

$ cargo run --release -- input_file_path  output_file_path -o FILE

```

## 例子

### 输入imp语言

```
x = 1;
y = 1;
x = x + y
```


### 输出一阶逻辑公式

```
D = {0, 1, 2}
V = {x, y}
R =  {
    x = 1 & y = 1 & pc = m & pc' = l1
    x' = x + y & same{V/{x}} & pc = l1 pc' = m'
}
```

### 输出KS

```
KS  = (S, S0, R, L)

S = {
    <0, 0>, <1, 0>, <2, 0>, <0, 1>, <1, 1>, <2, 1>, <0, 2>, <1, 2>, <2, 2>
}

S0 = {<1, 1>}

R(x, y, x', y') = {
    x' = (x + y) mod 3 & y' = y
}

L(S0) = S1
```

### 代码输出KS图
```text
D = {0,1,2,}
S = D x D = {(0,0,),(1,0,),(2,0,),(0,1,),(1,1,),(2,1,),(0,2,),(1,2,),(2,2,),}
s0 = (1,1,)
R = {((1,1,) -> (2,1,)),}
L = {((1,1,) -> (2,1,)),}
```

### 输入的json文件

```json
[
  {
    "payloadData" : {
      "initialValue": [
        {"name": "x", "value": "1"},
        {"name": "y", "value": "1"}
      ]
    },
    "pcFirst": "m",
    "pcLast": "l1"
  },
  {
    "payloadData" : {
      "exp": {
        "expression": "x = x + y",
        "same" : {
          "div":	"x"
        }
      }
    },
    "pcFirst": "l1",
    "pcLast": "m'"
  }
]
```

### 输出的json文件

```json
{
  "D": [
    {"num": "0"},
    {"num": "1"}
  ],
  "S": [
    {"first": "1", "second": "1"},
    {"first": "2", "second": "1"},
  ],
  "S0" : {
    "first": "1",
    "second": "1"
  },
  "R": [
    {
      "key": {
        "fist": "1","second": "1"
      },
      "value": {
        "first": "2", "second": "1"
      }
    }
  ],
  "L": [
    {
      "key": {
        "fist": "1","second": "1"
      },
      "value": {
        "first": "2", "second": "1"
      }
    }
  ]
}
```

## 输出txt的S， R文本
```text
## R 关系
1,1
2,1

## S 状态集合
0,0
1,0
2,0
0,1
1,1
2,1
0,2
1,2
2,2
```